package com.leahx_y2k.galoblender_updated;

import com.mojang.datafixers.util.Pair;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Climate;
import net.minecraftforge.fml.event.lifecycle.ParallelDispatchEvent;
import net.orcinus.galosphere.Galosphere;
import net.orcinus.galosphere.init.GBiomes;
import net.orcinus.galosphere.util.BiomeReagentHandler;
import terrablender.api.Region;
import terrablender.api.RegionType;
import terrablender.api.Regions;

import java.util.function.Consumer;

public class GaloblenderRegion extends Region {

    public GaloblenderRegion() {
        super(Galosphere.id("biome_provider"), RegionType.OVERWORLD, GaloblenderConfig.COMMON.galosphereCavesWeight.get());
    }

    @Override
    public void addBiomes(Registry<Biome> registry, Consumer<Pair<Climate.ParameterPoint, ResourceKey<Biome>>> mapper) {
        mapper.accept(Pair.of(BiomeReagentHandler.CRYSTAL_CANYONS_PARAMETER, GBiomes.CRYSTAL_CANYONS));
        mapper.accept(Pair.of(BiomeReagentHandler.LICHEN_CAVES_PARAMETER, GBiomes.LICHEN_CAVES));

        this.addModifiedVanillaOverworldBiomes(mapper, b -> { });
    }

    public void init(ParallelDispatchEvent event) {
        event.enqueueWork(() -> Regions.register(new GaloblenderRegion()));
    }
}
